package ru.maxim

fun printAutomat(automat : Array<Array<Int>>) {
    for(i in automat.indices){
        for (j in automat[0].indices) {
            print(automat[i][j].toString() + " ")
        }
        println()
    }
}