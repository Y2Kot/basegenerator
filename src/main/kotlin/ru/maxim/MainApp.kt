package ru.maxim

const val AUTOMAT_SIZE = 10
const val THREAD_LIFE_TIME_IN_SEC = 5
const val THREAD_COUNT = 32
const val DATABASE_SIZE = 10

fun main(args: Array<String>) {
    val task = MultiTaskGenerator()
    task.makeDataBase()
}