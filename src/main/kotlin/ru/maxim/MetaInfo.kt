package ru.maxim

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.nio.file.Files
import java.nio.file.Paths

class MetaInfo (private val  index : Int, private val automat: Automat) {
    private lateinit var automats : File
    private lateinit var greeds : File
    private lateinit var greedsCount : File

    fun saveMetaInfo() {
        initFolders()
        saveAutomat()
        saveGreedCount()
        saveGreeds()
    }

    private fun saveGreeds() {
        val writer = BufferedWriter(FileWriter(greeds))
        for (i in 0 until automat.xGreeds.size) {
            var xGreeds = ""
            var yGreeds = ""
            for (x in 0 until automat.xGreeds[i].size)
                xGreeds += "${automat.xGreeds[i][x]} "
            for (y in 0 until automat.yGreeds[i].size)
                yGreeds += "${automat.yGreeds[i][y]} "
            writer.write("X: $xGreeds\n")
            writer.write("Y: $yGreeds\n")
            writer.newLine()
        }
        writer.flush()
        writer.close()
    }

    private fun saveGreedCount() {
        val writer = BufferedWriter(FileWriter(greedsCount))
        writer.write(automat.greedCount.toString())
        writer.flush()
        writer.close()
    }

    private fun saveAutomat() {
        val writer = BufferedWriter(FileWriter(automats))
        for (row in automat.automat.indices) {
            var line = ""
            for (i in automat.automat[0].indices) {
                line += "${automat.automat[row][i]} "
            }
            writer.write(line + "\n")
        }
        writer.flush()
        writer.close()
    }

    private fun initFolders() {
        val automatNumber = "Automat-$index"
        val automatDirs = "${System.getProperty("user.dir")}//Automats//$automatNumber//"
        Files.createDirectories(Paths.get(automatDirs))

        automats = File("${automatDirs}matrix-$index")
        greeds = File("${automatDirs}greeds-$index")
        greedsCount = File("${automatDirs}greedsCount-$index")
    }
}