package ru.maxim

import java.util.concurrent.ConcurrentHashMap

class MultiTaskGenerator {
    private var base = arrayOfNulls<Automat>(DATABASE_SIZE)
    private var tasks = ArrayList<Thread>()
    private var taskResult = ConcurrentHashMap<Automat, Boolean>()

    @Volatile private var automatReady = 0

    @Volatile private var taskComplete = false
    get() {
        while (!field) {
        Thread.sleep(200)
        }
        return field
    }

    private fun initThreads() {
        for (i in 0 until THREAD_COUNT) oneTask(i)
    }

    private fun oneTask(index : Int) {
        tasks.add(index, Thread {
            val generatorTask = GeneratorTask()
            taskResult[generatorTask.generateTask()] = generatorTask.forceStop
        })
        tasks[index].start()
    }

    fun makeDataBase() {
        Thread {
            initThreads()
            while (automatReady < DATABASE_SIZE) {
                for (i in 0 until (tasks.size)) {
                    if (automatReady == DATABASE_SIZE) {
                        clearAllTasks()
                        break
                    }

                    if (tasks[i].isAlive) continue

                    if (!tasks[i].isAlive) {
                        val mapSet = taskResult.entries
                        val iterator = mapSet.iterator()
                        while (iterator.hasNext()) {
                            val mapEntry = iterator.next()
                            if (!mapEntry.value) {
                                if (automatReady < DATABASE_SIZE) {
                                    base[automatReady] = mapEntry.key
                                    MetaInfo(automatReady, mapEntry.key).saveMetaInfo()
                                }
                                println("automat №$automatReady added")
                                automatReady++
                                iterator.remove()
                                tasks.removeAt(i)
                                if (automatReady < DATABASE_SIZE) oneTask(i)
                            } else {
                                iterator.remove()
                                tasks.removeAt(i)
                                if (automatReady < DATABASE_SIZE) oneTask(i)
                            }
                        }
                    }
                }
            }
        }.start()
        taskComplete = true
    }
    private fun clearAllTasks() {
        tasks.clear()
    }
}