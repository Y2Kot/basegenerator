package ru.maxim

import java.util.*

fun Random.nextInt(range: IntRange): Int {
    return range.start + nextInt(range.last - range.start)
}

class RandomNumbers(private val automatSize: Int) {

    fun returnGreedSize() : Int {
        val multiplier : Int = Random().nextInt(1 .. 4)
        val greedSize = automatSize / multiplier
        return if (automatSize / multiplier < 2) 1 else Random().nextInt(1 .. greedSize)
    }

    fun returnGreedCount() : Int {
        return Random().nextInt(10 .. 200)
    }

    fun returnLineOrColumn(): Int {
        return Random().nextInt(0 .. automatSize)
    }
}