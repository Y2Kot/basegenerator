package ru.maxim

import java.util.concurrent.TimeUnit

class GeneratorTask {
    private val greedCount = RandomNumbers(AUTOMAT_SIZE).returnGreedCount()
    private val taskLifeTime = TimeUnit.SECONDS.toMillis(THREAD_LIFE_TIME_IN_SEC.toLong())

    @Volatile private var stopGeneration = false
    @Volatile var forceStop = false
    private lateinit var automat : Automat

    fun generateTask() : Automat {
        Thread {
            var limit = 0
            while (limit < taskLifeTime) {
                if (stopGeneration) break
                limit += 200
                Thread.sleep(200)
            }
            if (!stopGeneration) forceStop = true
            stopGeneration = true
        }.start()

        do {
            automat = Automat(AUTOMAT_SIZE)
            if (stopGeneration) break
            generateAutomat(automat)
        } while (!automat.isCorrect())
        stopGeneration = true
        return automat
    }
    private fun generateAutomat(candidate: Automat) {
        for (i in 1 .. greedCount) {
            candidate.addGreed()
        }
    }
}