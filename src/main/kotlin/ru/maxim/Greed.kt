package ru.maxim

class Greed (private val automatSize: Int) {
    val x: Array<Int> = fillArray()
    val y: Array<Int> = fillArray()

    private fun fillArray() : Array<Int> {
        val array: Array<Int> = Array(RandomNumbers(automatSize).returnGreedSize(), { -1 })
        var i = 0
        while (-1 in array) {
            val tempInt = RandomNumbers(automatSize).returnLineOrColumn()
            if (tempInt !in array) {
                array[i] = tempInt
                i++
            }
        }
        array.sort()
        return array
    }
}