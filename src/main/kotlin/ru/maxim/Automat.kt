package ru.maxim

class Automat (private val size : Int) {
    val automat = Array(size, { Array(size, { 0 }) })

    val xGreeds = ArrayList<Array<Int>>()
    val yGreeds = ArrayList<Array<Int>>()
    var greedCount : Int = 0

    fun addGreed() {
        val greed = Greed(size)
        xGreeds.add(greed.x)
        yGreeds.add(greed.y)

        for (y in greed.y) {
            for (x in greed.y) {
                automat[y][x] = 1
            }
        }
        greedCount++
    }

    fun isCorrect() : Boolean {
        val stateMass = arrayOf(checkMatrixRows(), checkMatrixColumns())
        return (false !in stateMass)
    }

    private fun checkMatrixRows() : Boolean {
        val zeroRow = Array(size, { 0 })
        for (rows in automat.withIndex()) {
            if (rows.value.contentEquals(zeroRow)) return false
            val currentRow = rows.value
            var counter = 0
            for (mRows in automat.withIndex()) {
                if (currentRow.contentEquals(mRows.value)) counter++
            }
            if (counter > 1) return false
        }
        return true
    }

    private fun checkMatrixColumns() : Boolean {
        val invertAutomat = automat
        val zeroRow = Array(size, { 0 })

        for (i in automat.indices) {
            for (j in automat.indices) {
                invertAutomat[i][j] = automat[j][i]
            }
        }

        for (rows in automat.withIndex()) {
            if (rows.value.contentEquals(zeroRow)) return false
            val currentRow = rows.value
            var counter = 0
            for (mRows in automat.withIndex()) {
                if (currentRow.contentEquals(mRows.value)) counter++
            }
            if (counter > 1) return false
        }
        return true
    }
}